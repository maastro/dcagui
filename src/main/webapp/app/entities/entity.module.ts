import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { DcaguiPatientModule } from './patient/patient.module';
import { DcaguiFhirTransactionLogModule } from './fhir-transaction-log/fhir-transaction-log.module';
import { DcaguiRnvPlanModule } from './rnv-plan/rnv-plan.module';
import { DcaguiPlanObservationModule } from './plan-observation/plan-observation.module';
import { DcaguiPatientObservationModule } from './patient-observation/patient-observation.module';
import { DcaguiDicaRegistrationEntityModule } from './dica-registration-entity/dica-registration-entity.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        DcaguiPatientModule,
        DcaguiFhirTransactionLogModule,
        DcaguiRnvPlanModule,
        DcaguiPlanObservationModule,
        DcaguiPatientObservationModule,
        DcaguiDicaRegistrationEntityModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DcaguiEntityModule {}
