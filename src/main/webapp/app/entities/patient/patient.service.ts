import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { DateUtils } from 'ng-jhipster';

import { Patient } from './patient.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PatientService {

    private resourceUrl = 'dcaservice/api/patients';

    constructor(private http: Http, private dateUtils: DateUtils) { }

    create(patient: Patient): Observable<Patient> {
        const copy = this.convert(patient);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(patient: Patient): Observable<Patient> {
        const copy = this.convert(patient);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<Patient> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.dateOfBirth = this.dateUtils
            .convertLocalDateFromServer(entity.dateOfBirth);
    }

    private convert(patient: Patient): Patient {
        const copy: Patient = Object.assign({}, patient);
        copy.dateOfBirth = this.dateUtils
            .convertLocalDateToServer(patient.dateOfBirth);
        return copy;
    }
}
