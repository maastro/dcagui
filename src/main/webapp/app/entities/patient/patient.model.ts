
const enum Gender {
    'MALE',
    'FEMALE',
    'OTHER'

};
import { RnvPlan } from '../rnv-plan';
import { PatientObservation } from '../patient-observation';
export class Patient {
    constructor(
        public id?: number,
        public patientUid?: string,
        public bsn?: string,
        public nameInitials?: string,
        public nameFamilyPrefix?: string,
        public nameFamily?: string,
        public gender?: Gender,
        public dateOfBirth?: any,
        public rnvPlan?: RnvPlan,
        public patientObservations?: PatientObservation,
    ) {
    }
}
