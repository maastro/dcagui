import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Patient } from './patient.model';
import { PatientService } from './patient.service';
@Injectable()
export class PatientPopupService {
    private isOpen = false;
    constructor(
        private modalService: NgbModal,
        private router: Router,
        private patientService: PatientService

    ) {}

    open(component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.patientService.find(id).subscribe((patient) => {
                if (patient.dateOfBirth) {
                    patient.dateOfBirth = {
                        year: patient.dateOfBirth.getFullYear(),
                        month: patient.dateOfBirth.getMonth() + 1,
                        day: patient.dateOfBirth.getDate()
                    };
                }
                this.patientModalRef(component, patient);
            });
        } else {
            return this.patientModalRef(component, new Patient());
        }
    }

    patientModalRef(component: Component, patient: Patient): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.patient = patient;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
