import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DcaguiSharedModule } from '../../shared';
import {
    PatientObservationService,
    PatientObservationPopupService,
    PatientObservationComponent,
    PatientObservationDetailComponent,
    PatientObservationDialogComponent,
    PatientObservationPopupComponent,
    PatientObservationDeletePopupComponent,
    PatientObservationDeleteDialogComponent,
    patientObservationRoute,
    patientObservationPopupRoute,
    PatientObservationResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...patientObservationRoute,
    ...patientObservationPopupRoute,
];

@NgModule({
    imports: [
        DcaguiSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        PatientObservationComponent,
        PatientObservationDetailComponent,
        PatientObservationDialogComponent,
        PatientObservationDeleteDialogComponent,
        PatientObservationPopupComponent,
        PatientObservationDeletePopupComponent,
    ],
    entryComponents: [
        PatientObservationComponent,
        PatientObservationDialogComponent,
        PatientObservationPopupComponent,
        PatientObservationDeleteDialogComponent,
        PatientObservationDeletePopupComponent,
    ],
    providers: [
        PatientObservationService,
        PatientObservationPopupService,
        PatientObservationResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DcaguiPatientObservationModule {}
