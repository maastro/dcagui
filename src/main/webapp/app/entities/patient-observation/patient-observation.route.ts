import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { PaginationUtil } from 'ng-jhipster';

import { PatientObservationComponent } from './patient-observation.component';
import { PatientObservationDetailComponent } from './patient-observation-detail.component';
import { PatientObservationPopupComponent } from './patient-observation-dialog.component';
import { PatientObservationDeletePopupComponent } from './patient-observation-delete-dialog.component';

import { Principal } from '../../shared';

@Injectable()
export class PatientObservationResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: PaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const patientObservationRoute: Routes = [
    {
        path: 'patient-observation',
        component: PatientObservationComponent,
        resolve: {
            'pagingParams': PatientObservationResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dcaguiApp.patientObservation.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'patient-observation/:id',
        component: PatientObservationDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dcaguiApp.patientObservation.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const patientObservationPopupRoute: Routes = [
    {
        path: 'patient-observation-new',
        component: PatientObservationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dcaguiApp.patientObservation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'patient-observation/:id/edit',
        component: PatientObservationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dcaguiApp.patientObservation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'patient-observation/:id/delete',
        component: PatientObservationDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dcaguiApp.patientObservation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
