export * from './patient-observation.model';
export * from './patient-observation-popup.service';
export * from './patient-observation.service';
export * from './patient-observation-dialog.component';
export * from './patient-observation-delete-dialog.component';
export * from './patient-observation-detail.component';
export * from './patient-observation.component';
export * from './patient-observation.route';
