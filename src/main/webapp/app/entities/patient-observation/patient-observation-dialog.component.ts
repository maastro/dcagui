import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, AlertService } from 'ng-jhipster';

import { PatientObservation } from './patient-observation.model';
import { PatientObservationPopupService } from './patient-observation-popup.service';
import { PatientObservationService } from './patient-observation.service';
import { Patient, PatientService } from '../patient';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-patient-observation-dialog',
    templateUrl: './patient-observation-dialog.component.html'
})
export class PatientObservationDialogComponent implements OnInit {

    patientObservation: PatientObservation;
    authorities: any[];
    isSaving: boolean;

    patients: Patient[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: AlertService,
        private patientObservationService: PatientObservationService,
        private patientService: PatientService,
        private eventManager: EventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.patientService.query({page: 0, size: 10000})
            .subscribe((res: ResponseWrapper) => { this.patients = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.patientObservation.id !== undefined) {
            this.subscribeToSaveResponse(
                this.patientObservationService.update(this.patientObservation), false);
        } else {
            this.subscribeToSaveResponse(
                this.patientObservationService.create(this.patientObservation), true);
        }
    }

    private subscribeToSaveResponse(result: Observable<PatientObservation>, isCreated: boolean) {
        result.subscribe((res: PatientObservation) =>
            this.onSaveSuccess(res, isCreated), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: PatientObservation, isCreated: boolean) {
        this.alertService.success(
            isCreated ? 'dcaguiApp.patientObservation.created'
            : 'dcaguiApp.patientObservation.updated',
            { param : result.id }, null);

        this.eventManager.broadcast({ name: 'patientObservationListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackPatientById(index: number, item: Patient) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-patient-observation-popup',
    template: ''
})
export class PatientObservationPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private patientObservationPopupService: PatientObservationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.modalRef = this.patientObservationPopupService
                    .open(PatientObservationDialogComponent, params['id']);
            } else {
                this.modalRef = this.patientObservationPopupService
                    .open(PatientObservationDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
