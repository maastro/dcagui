import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AlertService, EventManager } from 'ng-jhipster';

import { PatientObservation } from './patient-observation.model';
import { PatientObservationPopupService } from './patient-observation-popup.service';
import { PatientObservationService } from './patient-observation.service';

@Component({
    selector: 'jhi-patient-observation-delete-dialog',
    templateUrl: './patient-observation-delete-dialog.component.html'
})
export class PatientObservationDeleteDialogComponent {

    patientObservation: PatientObservation;

    constructor(
        private patientObservationService: PatientObservationService,
        public activeModal: NgbActiveModal,
        private alertService: AlertService,
        private eventManager: EventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.patientObservationService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'patientObservationListModification',
                content: 'Deleted an patientObservation'
            });
            this.activeModal.dismiss(true);
        });
        this.alertService.success('dcaguiApp.patientObservation.deleted', { param : id }, null);
    }
}

@Component({
    selector: 'jhi-patient-observation-delete-popup',
    template: ''
})
export class PatientObservationDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private patientObservationPopupService: PatientObservationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.patientObservationPopupService
                .open(PatientObservationDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
