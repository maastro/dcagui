import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { DateUtils } from 'ng-jhipster';

import { PatientObservation } from './patient-observation.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PatientObservationService {

    private resourceUrl = 'dcaservice/api/patient-observations';

    constructor(private http: Http, private dateUtils: DateUtils) { }

    create(patientObservation: PatientObservation): Observable<PatientObservation> {
        const copy = this.convert(patientObservation);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(patientObservation: PatientObservation): Observable<PatientObservation> {
        const copy = this.convert(patientObservation);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<PatientObservation> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.observationDate = this.dateUtils
            .convertDateTimeFromServer(entity.observationDate);
    }

    private convert(patientObservation: PatientObservation): PatientObservation {
        const copy: PatientObservation = Object.assign({}, patientObservation);

        copy.observationDate = this.dateUtils.toDate(patientObservation.observationDate);
        return copy;
    }
}
