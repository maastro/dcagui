import { Patient } from '../patient';
export class PatientObservation {
    constructor(
        public id?: number,
        public observationIdentifier?: string,
        public observationValue?: string,
        public observationDate?: any,
        public patient?: Patient,
    ) {
    }
}
