import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { PatientObservation } from './patient-observation.model';
import { PatientObservationService } from './patient-observation.service';
@Injectable()
export class PatientObservationPopupService {
    private isOpen = false;
    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private patientObservationService: PatientObservationService

    ) {}

    open(component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.patientObservationService.find(id).subscribe((patientObservation) => {
                patientObservation.observationDate = this.datePipe
                    .transform(patientObservation.observationDate, 'yyyy-MM-ddThh:mm');
                this.patientObservationModalRef(component, patientObservation);
            });
        } else {
            return this.patientObservationModalRef(component, new PatientObservation());
        }
    }

    patientObservationModalRef(component: Component, patientObservation: PatientObservation): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.patientObservation = patientObservation;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
