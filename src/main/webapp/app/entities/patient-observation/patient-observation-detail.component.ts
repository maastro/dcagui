import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { EventManager  } from 'ng-jhipster';

import { PatientObservation } from './patient-observation.model';
import { PatientObservationService } from './patient-observation.service';

@Component({
    selector: 'jhi-patient-observation-detail',
    templateUrl: './patient-observation-detail.component.html'
})
export class PatientObservationDetailComponent implements OnInit, OnDestroy {

    patientObservation: PatientObservation;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: EventManager,
        private patientObservationService: PatientObservationService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPatientObservations();
    }

    load(id) {
        this.patientObservationService.find(id).subscribe((patientObservation) => {
            this.patientObservation = patientObservation;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPatientObservations() {
        this.eventSubscriber = this.eventManager.subscribe(
            'patientObservationListModification',
            (response) => this.load(this.patientObservation.id)
        );
    }
}
