import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { EventManager  } from 'ng-jhipster';

import { RnvPlan } from './rnv-plan.model';
import { RnvPlanService } from './rnv-plan.service';

@Component({
    selector: 'jhi-rnv-plan-detail',
    templateUrl: './rnv-plan-detail.component.html'
})
export class RnvPlanDetailComponent implements OnInit, OnDestroy {

    rnvPlan: RnvPlan;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: EventManager,
        private rnvPlanService: RnvPlanService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInRnvPlans();
    }

    load(id) {
        this.rnvPlanService.find(id).subscribe((rnvPlan) => {
            this.rnvPlan = rnvPlan;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInRnvPlans() {
        this.eventSubscriber = this.eventManager.subscribe(
            'rnvPlanListModification',
            (response) => this.load(this.rnvPlan.id)
        );
    }
}
