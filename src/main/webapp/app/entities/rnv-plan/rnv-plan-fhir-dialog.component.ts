import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AlertService, EventManager } from 'ng-jhipster';

import { RnvPlan } from './rnv-plan.model';
import { RnvPlanPopupService } from './rnv-plan-popup.service';
import { RnvPlanService } from './rnv-plan.service';

@Component({
    selector: 'jhi-rnv-plan-fhir-dialog',
    templateUrl: './rnv-plan-fhir-dialog.component.html'
})
export class RnvPlanFhirDialogComponent {

    rnvPlan: RnvPlan;

    constructor(
        private rnvPlanService: RnvPlanService,
        public activeModal: NgbActiveModal,
        private alertService: AlertService,
        private eventManager: EventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmFhir(id: number) {
        this.rnvPlanService.fhir(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'rnvPlanListModification',
                content: 'Sent rnvPlan over Fhir'
            });
            this.activeModal.dismiss(true);
            this.alertService.success('dcaguiApp.rnvPlan.fhir.sent', { param : id }, null);
        });
    }
}

@Component({
    selector: 'jhi-rnv-plan-fhir-popup',
    template: ''
})
export class RnvPlanFhirPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private rnvPlanPopupService: RnvPlanPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.rnvPlanPopupService
                .open(RnvPlanFhirDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
