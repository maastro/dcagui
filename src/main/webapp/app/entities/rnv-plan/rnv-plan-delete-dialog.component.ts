import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AlertService, EventManager } from 'ng-jhipster';

import { RnvPlan } from './rnv-plan.model';
import { RnvPlanPopupService } from './rnv-plan-popup.service';
import { RnvPlanService } from './rnv-plan.service';

@Component({
    selector: 'jhi-rnv-plan-delete-dialog',
    templateUrl: './rnv-plan-delete-dialog.component.html'
})
export class RnvPlanDeleteDialogComponent {

    rnvPlan: RnvPlan;

    constructor(
        private rnvPlanService: RnvPlanService,
        public activeModal: NgbActiveModal,
        private alertService: AlertService,
        private eventManager: EventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.rnvPlanService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'rnvPlanListModification',
                content: 'Deleted an rnvPlan'
            });
            this.activeModal.dismiss(true);
        });
        this.alertService.success('dcaguiApp.rnvPlan.deleted', { param : id }, null);
    }
}

@Component({
    selector: 'jhi-rnv-plan-delete-popup',
    template: ''
})
export class RnvPlanDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private rnvPlanPopupService: RnvPlanPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.rnvPlanPopupService
                .open(RnvPlanDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
