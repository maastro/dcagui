import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { PaginationUtil } from 'ng-jhipster';

import { RnvPlanComponent } from './rnv-plan.component';
import { RnvPlanDetailComponent } from './rnv-plan-detail.component';
import { RnvPlanPopupComponent } from './rnv-plan-dialog.component';
import { RnvPlanDeletePopupComponent } from './rnv-plan-delete-dialog.component';
import { RnvPlanFhirPopupComponent } from './rnv-plan-fhir-dialog.component';
import { RnvPlanFhirAllPopupComponent } from './rnv-plan-fhir-all-dialog.component';

import { Principal } from '../../shared';

@Injectable()
export class RnvPlanResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: PaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const rnvPlanRoute: Routes = [
    {
        path: 'rnv-plan',
        component: RnvPlanComponent,
        resolve: {
            'pagingParams': RnvPlanResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dcaguiApp.rnvPlan.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'rnv-plan/:id',
        component: RnvPlanDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dcaguiApp.rnvPlan.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const rnvPlanPopupRoute: Routes = [
    {
        path: 'rnv-plan-new',
        component: RnvPlanPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dcaguiApp.rnvPlan.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'rnv-plan/:id/edit',
        component: RnvPlanPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dcaguiApp.rnvPlan.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'rnv-plan/:id/delete',
        component: RnvPlanDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dcaguiApp.rnvPlan.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'rnv-plan/:id/fhir',
        component: RnvPlanFhirPopupComponent,
        data: {
              authorities: ['ROLE_USER'],
              pageTitle: 'dcaguiApp.rnvPlan.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'rnv-plan-fhir-all',
        component: RnvPlanFhirAllPopupComponent,
        data: {
              authorities: ['ROLE_USER'],
              pageTitle: 'dcaguiApp.rnvPlan.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
