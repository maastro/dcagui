import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AlertService, EventManager } from 'ng-jhipster';

import { RnvPlan } from './rnv-plan.model';
import { RnvPlanPopupService } from './rnv-plan-popup.service';
import { RnvPlanService } from './rnv-plan.service';

@Component({
    selector: 'jhi-rnv-plan-fhir-all-dialog',
    templateUrl: './rnv-plan-fhir-all-dialog.component.html'
})
export class RnvPlanFhirAllDialogComponent {

    rnvPlan: RnvPlan;

    constructor(
        private rnvPlanService: RnvPlanService,
        public activeModal: NgbActiveModal,
        private alertService: AlertService,
        private eventManager: EventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmFhirAll() {
        this.rnvPlanService.fhirAll().subscribe((response) => {
            this.eventManager.broadcast({
                name: 'rnvPlanListModification',
                content: 'Sent all rnvPlans over Fhir'
            });
            this.activeModal.dismiss(true);
            this.alertService.success('dcaguiApp.rnvPlan.fhirAll.sent', {}, null);
        });
    }
}

@Component({
    selector: 'jhi-rnv-plan-fhir-popup',
    template: ''
})
export class RnvPlanFhirAllPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private rnvPlanPopupService: RnvPlanPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.rnvPlanPopupService
                .open(RnvPlanFhirAllDialogComponent, );
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
