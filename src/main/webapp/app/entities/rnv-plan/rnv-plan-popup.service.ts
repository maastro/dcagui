import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { RnvPlan } from './rnv-plan.model';
import { RnvPlanService } from './rnv-plan.service';
@Injectable()
export class RnvPlanPopupService {
    private isOpen = false;
    constructor(
        private modalService: NgbModal,
        private router: Router,
        private rnvPlanService: RnvPlanService

    ) {}

    open(component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.rnvPlanService.find(id).subscribe((rnvPlan) => {
                if (rnvPlan.dateCreated) {
                    rnvPlan.dateCreated = {
                        year: rnvPlan.dateCreated.getFullYear(),
                        month: rnvPlan.dateCreated.getMonth() + 1,
                        day: rnvPlan.dateCreated.getDate()
                    };
                }
                this.rnvPlanModalRef(component, rnvPlan);
            });
        } else {
            return this.rnvPlanModalRef(component, new RnvPlan());
        }
    }

    rnvPlanModalRef(component: Component, rnvPlan: RnvPlan): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.rnvPlan = rnvPlan;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
