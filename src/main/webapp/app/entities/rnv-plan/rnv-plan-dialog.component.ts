import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, AlertService } from 'ng-jhipster';

import { RnvPlan } from './rnv-plan.model';
import { RnvPlanPopupService } from './rnv-plan-popup.service';
import { RnvPlanService } from './rnv-plan.service';
import { Patient, PatientService } from '../patient';
import { DicaRegistrationEntity, DicaRegistrationEntityService } from '../dica-registration-entity';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-rnv-plan-dialog',
    templateUrl: './rnv-plan-dialog.component.html'
})
export class RnvPlanDialogComponent implements OnInit {

    rnvPlan: RnvPlan;
    authorities: any[];
    isSaving: boolean;

    patients: Patient[];

    dicaregistrationentities: DicaRegistrationEntity[];
    dateCreatedDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: AlertService,
        private rnvPlanService: RnvPlanService,
        private patientService: PatientService,
        private dicaRegistrationEntityService: DicaRegistrationEntityService,
        private eventManager: EventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.patientService.query({page: 0, size: 10000})
            .subscribe((res: ResponseWrapper) => { this.patients = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.dicaRegistrationEntityService.query()
            .subscribe((res: ResponseWrapper) => { this.dicaregistrationentities = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.rnvPlan.id !== undefined) {
            this.subscribeToSaveResponse(
                this.rnvPlanService.update(this.rnvPlan), false);
        } else {
            this.subscribeToSaveResponse(
                this.rnvPlanService.create(this.rnvPlan), true);
        }
    }

    private subscribeToSaveResponse(result: Observable<RnvPlan>, isCreated: boolean) {
        result.subscribe((res: RnvPlan) =>
            this.onSaveSuccess(res, isCreated), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: RnvPlan, isCreated: boolean) {
        this.alertService.success(
            isCreated ? 'dcaguiApp.rnvPlan.created'
            : 'dcaguiApp.rnvPlan.updated',
            { param : result.id }, null);

        this.eventManager.broadcast({ name: 'rnvPlanListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackPatientById(index: number, item: Patient) {
        return item.id;
    }

    trackDicaRegistrationEntityById(index: number, item: DicaRegistrationEntity) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-rnv-plan-popup',
    template: ''
})
export class RnvPlanPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private rnvPlanPopupService: RnvPlanPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.modalRef = this.rnvPlanPopupService
                    .open(RnvPlanDialogComponent, params['id']);
            } else {
                this.modalRef = this.rnvPlanPopupService
                    .open(RnvPlanDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
