import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DcaguiSharedModule } from '../../shared';
import {
    RnvPlanService,
    RnvPlanPopupService,
    RnvPlanComponent,
    RnvPlanDetailComponent,
    RnvPlanDialogComponent,
    RnvPlanPopupComponent,
    RnvPlanDeletePopupComponent,
    RnvPlanDeleteDialogComponent,
    RnvPlanFhirPopupComponent,
    RnvPlanFhirDialogComponent,
    RnvPlanFhirAllPopupComponent,
    RnvPlanFhirAllDialogComponent,
    rnvPlanRoute,
    rnvPlanPopupRoute,
    RnvPlanResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...rnvPlanRoute,
    ...rnvPlanPopupRoute,
];

@NgModule({
    imports: [
        DcaguiSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        RnvPlanComponent,
        RnvPlanDetailComponent,
        RnvPlanDialogComponent,
        RnvPlanDeleteDialogComponent,
        RnvPlanPopupComponent,
        RnvPlanDeletePopupComponent,
        RnvPlanFhirPopupComponent,
        RnvPlanFhirDialogComponent,
        RnvPlanFhirAllPopupComponent,
        RnvPlanFhirAllDialogComponent,
    ],
    entryComponents: [
        RnvPlanComponent,
        RnvPlanDialogComponent,
        RnvPlanPopupComponent,
        RnvPlanDeleteDialogComponent,
        RnvPlanDeletePopupComponent,
        RnvPlanFhirPopupComponent,
        RnvPlanFhirDialogComponent,
        RnvPlanFhirAllPopupComponent,
        RnvPlanFhirAllDialogComponent,
    ],
    providers: [
        RnvPlanService,
        RnvPlanPopupService,
        RnvPlanResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DcaguiRnvPlanModule {}
