import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { DateUtils } from 'ng-jhipster';

import { RnvPlan } from './rnv-plan.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class RnvPlanService {

    private resourceUrl = 'dcaservice/api/rnv-plans';

    constructor(private http: Http, private dateUtils: DateUtils) { }

    create(rnvPlan: RnvPlan): Observable<RnvPlan> {
        const copy = this.convert(rnvPlan);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(rnvPlan: RnvPlan): Observable<RnvPlan> {
        const copy = this.convert(rnvPlan);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<RnvPlan> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        options.search.set('uncoupled', req.uncoupled);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    fhir(id: number): Observable<Response> {
            return this.http.get(`${this.resourceUrl}/${id}/fhir`);
    }

    fhirAll(): Observable<Response> {
            return this.http.get(`${this.resourceUrl}/fhir`);
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.dateCreated = this.dateUtils
            .convertLocalDateFromServer(entity.dateCreated);
    }

    private convert(rnvPlan: RnvPlan): RnvPlan {
        const copy: RnvPlan = Object.assign({}, rnvPlan);
        copy.dateCreated = this.dateUtils
            .convertLocalDateToServer(rnvPlan.dateCreated);
        return copy;
    }
}
