import { Patient } from '../patient';
import { PlanObservation } from '../plan-observation';
import { DicaRegistrationEntity } from '../dica-registration-entity';
export class RnvPlan {
    constructor(
        public id?: number,
        public planLabel?: string,
        public planSopInstanceUid?: string,
        public studyInstanceUid?: string,
        public dateCreated?: any,
        public patient?: Patient,
        public planObservations?: PlanObservation,
        public dicaRegistrations?: DicaRegistrationEntity,
    ) {
    }
}
