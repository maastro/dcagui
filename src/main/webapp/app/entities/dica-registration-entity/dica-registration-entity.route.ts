import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { PaginationUtil } from 'ng-jhipster';

import { DicaRegistrationEntityComponent } from './dica-registration-entity.component';
import { DicaRegistrationEntityDetailComponent } from './dica-registration-entity-detail.component';
import { DicaRegistrationEntityPopupComponent } from './dica-registration-entity-dialog.component';
import { DicaRegistrationEntityDeletePopupComponent } from './dica-registration-entity-delete-dialog.component';

import { Principal } from '../../shared';

@Injectable()
export class DicaRegistrationEntityResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: PaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const dicaRegistrationEntityRoute: Routes = [
    {
        path: 'dica-registration-entity',
        component: DicaRegistrationEntityComponent,
        resolve: {
            'pagingParams': DicaRegistrationEntityResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dcaguiApp.dicaRegistrationEntity.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'dica-registration-entity/:id',
        component: DicaRegistrationEntityDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dcaguiApp.dicaRegistrationEntity.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const dicaRegistrationEntityPopupRoute: Routes = [
    {
        path: 'dica-registration-entity-new',
        component: DicaRegistrationEntityPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dcaguiApp.dicaRegistrationEntity.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'dica-registration-entity/:id/edit',
        component: DicaRegistrationEntityPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dcaguiApp.dicaRegistrationEntity.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'dica-registration-entity/:id/delete',
        component: DicaRegistrationEntityDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dcaguiApp.dicaRegistrationEntity.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
