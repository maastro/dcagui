export * from './dica-registration-entity.model';
export * from './dica-registration-entity-popup.service';
export * from './dica-registration-entity.service';
export * from './dica-registration-entity-dialog.component';
export * from './dica-registration-entity-delete-dialog.component';
export * from './dica-registration-entity-detail.component';
export * from './dica-registration-entity.component';
export * from './dica-registration-entity.route';
