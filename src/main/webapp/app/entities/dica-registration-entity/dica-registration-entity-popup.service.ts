import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DicaRegistrationEntity } from './dica-registration-entity.model';
import { DicaRegistrationEntityService } from './dica-registration-entity.service';
@Injectable()
export class DicaRegistrationEntityPopupService {
    private isOpen = false;
    constructor(
        private modalService: NgbModal,
        private router: Router,
        private dicaRegistrationEntityService: DicaRegistrationEntityService

    ) {}

    open(component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.dicaRegistrationEntityService.find(id).subscribe((dicaRegistrationEntity) => {
                this.dicaRegistrationEntityModalRef(component, dicaRegistrationEntity);
            });
        } else {
            return this.dicaRegistrationEntityModalRef(component, new DicaRegistrationEntity());
        }
    }

    dicaRegistrationEntityModalRef(component: Component, dicaRegistrationEntity: DicaRegistrationEntity): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.dicaRegistrationEntity = dicaRegistrationEntity;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
