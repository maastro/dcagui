import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AlertService, EventManager } from 'ng-jhipster';

import { DicaRegistrationEntity } from './dica-registration-entity.model';
import { DicaRegistrationEntityPopupService } from './dica-registration-entity-popup.service';
import { DicaRegistrationEntityService } from './dica-registration-entity.service';

@Component({
    selector: 'jhi-dica-registration-entity-delete-dialog',
    templateUrl: './dica-registration-entity-delete-dialog.component.html'
})
export class DicaRegistrationEntityDeleteDialogComponent {

    dicaRegistrationEntity: DicaRegistrationEntity;

    constructor(
        private dicaRegistrationEntityService: DicaRegistrationEntityService,
        public activeModal: NgbActiveModal,
        private alertService: AlertService,
        private eventManager: EventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.dicaRegistrationEntityService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'dicaRegistrationEntityListModification',
                content: 'Deleted an dicaRegistrationEntity'
            });
            this.activeModal.dismiss(true);
        });
        this.alertService.success('dcaguiApp.dicaRegistrationEntity.deleted', { param : id }, null);
    }
}

@Component({
    selector: 'jhi-dica-registration-entity-delete-popup',
    template: ''
})
export class DicaRegistrationEntityDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dicaRegistrationEntityPopupService: DicaRegistrationEntityPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.dicaRegistrationEntityPopupService
                .open(DicaRegistrationEntityDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
