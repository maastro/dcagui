import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, AlertService } from 'ng-jhipster';

import { DicaRegistrationEntity } from './dica-registration-entity.model';
import { DicaRegistrationEntityPopupService } from './dica-registration-entity-popup.service';
import { DicaRegistrationEntityService } from './dica-registration-entity.service';
import { RnvPlan, RnvPlanService } from '../rnv-plan';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-dica-registration-entity-dialog',
    templateUrl: './dica-registration-entity-dialog.component.html'
})
export class DicaRegistrationEntityDialogComponent implements OnInit {

    dicaRegistrationEntity: DicaRegistrationEntity;
    authorities: any[];
    isSaving: boolean;

    rnvplans: RnvPlan[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: AlertService,
        private dicaRegistrationEntityService: DicaRegistrationEntityService,
        private rnvPlanService: RnvPlanService,
        private eventManager: EventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.rnvPlanService.query()
            .subscribe((res: ResponseWrapper) => { this.rnvplans = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.dicaRegistrationEntity.id !== undefined) {
            this.subscribeToSaveResponse(
                this.dicaRegistrationEntityService.update(this.dicaRegistrationEntity), false);
        } else {
            this.subscribeToSaveResponse(
                this.dicaRegistrationEntityService.create(this.dicaRegistrationEntity), true);
        }
    }

    private subscribeToSaveResponse(result: Observable<DicaRegistrationEntity>, isCreated: boolean) {
        result.subscribe((res: DicaRegistrationEntity) =>
            this.onSaveSuccess(res, isCreated), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: DicaRegistrationEntity, isCreated: boolean) {
        this.alertService.success(
            isCreated ? 'dcaguiApp.dicaRegistrationEntity.created'
            : 'dcaguiApp.dicaRegistrationEntity.updated',
            { param : result.id }, null);

        this.eventManager.broadcast({ name: 'dicaRegistrationEntityListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackRnvPlanById(index: number, item: RnvPlan) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-dica-registration-entity-popup',
    template: ''
})
export class DicaRegistrationEntityPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dicaRegistrationEntityPopupService: DicaRegistrationEntityPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.modalRef = this.dicaRegistrationEntityPopupService
                    .open(DicaRegistrationEntityDialogComponent, params['id']);
            } else {
                this.modalRef = this.dicaRegistrationEntityPopupService
                    .open(DicaRegistrationEntityDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
