import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { DicaRegistrationEntity } from './dica-registration-entity.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class DicaRegistrationEntityService {

    private resourceUrl = 'dcaservice/api/dica-registration-entities';

    constructor(private http: Http) { }

    create(dicaRegistrationEntity: DicaRegistrationEntity): Observable<DicaRegistrationEntity> {
        const copy = this.convert(dicaRegistrationEntity);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(dicaRegistrationEntity: DicaRegistrationEntity): Observable<DicaRegistrationEntity> {
        const copy = this.convert(dicaRegistrationEntity);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<DicaRegistrationEntity> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(dicaRegistrationEntity: DicaRegistrationEntity): DicaRegistrationEntity {
        const copy: DicaRegistrationEntity = Object.assign({}, dicaRegistrationEntity);
        return copy;
    }
}
