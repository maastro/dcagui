import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DcaguiSharedModule } from '../../shared';
import {
    DicaRegistrationEntityService,
    DicaRegistrationEntityPopupService,
    DicaRegistrationEntityComponent,
    DicaRegistrationEntityDetailComponent,
    DicaRegistrationEntityDialogComponent,
    DicaRegistrationEntityPopupComponent,
    DicaRegistrationEntityDeletePopupComponent,
    DicaRegistrationEntityDeleteDialogComponent,
    dicaRegistrationEntityRoute,
    dicaRegistrationEntityPopupRoute,
    DicaRegistrationEntityResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...dicaRegistrationEntityRoute,
    ...dicaRegistrationEntityPopupRoute,
];

@NgModule({
    imports: [
        DcaguiSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        DicaRegistrationEntityComponent,
        DicaRegistrationEntityDetailComponent,
        DicaRegistrationEntityDialogComponent,
        DicaRegistrationEntityDeleteDialogComponent,
        DicaRegistrationEntityPopupComponent,
        DicaRegistrationEntityDeletePopupComponent,
    ],
    entryComponents: [
        DicaRegistrationEntityComponent,
        DicaRegistrationEntityDialogComponent,
        DicaRegistrationEntityPopupComponent,
        DicaRegistrationEntityDeleteDialogComponent,
        DicaRegistrationEntityDeletePopupComponent,
    ],
    providers: [
        DicaRegistrationEntityService,
        DicaRegistrationEntityPopupService,
        DicaRegistrationEntityResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DcaguiDicaRegistrationEntityModule {}
