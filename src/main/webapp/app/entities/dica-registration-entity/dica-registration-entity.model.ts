
const enum DicaRegistration {
    'DLCA'

};
import { RnvPlan } from '../rnv-plan';
export class DicaRegistrationEntity {
    constructor(
        public id?: number,
        public dicaRegistration?: RnvPlan,
    ) {
    }
}
