import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { EventManager  } from 'ng-jhipster';

import { DicaRegistrationEntity } from './dica-registration-entity.model';
import { DicaRegistrationEntityService } from './dica-registration-entity.service';

@Component({
    selector: 'jhi-dica-registration-entity-detail',
    templateUrl: './dica-registration-entity-detail.component.html'
})
export class DicaRegistrationEntityDetailComponent implements OnInit, OnDestroy {

    dicaRegistrationEntity: DicaRegistrationEntity;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: EventManager,
        private dicaRegistrationEntityService: DicaRegistrationEntityService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDicaRegistrationEntities();
    }

    load(id) {
        this.dicaRegistrationEntityService.find(id).subscribe((dicaRegistrationEntity) => {
            this.dicaRegistrationEntity = dicaRegistrationEntity;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDicaRegistrationEntities() {
        this.eventSubscriber = this.eventManager.subscribe(
            'dicaRegistrationEntityListModification',
            (response) => this.load(this.dicaRegistrationEntity.id)
        );
    }
}
