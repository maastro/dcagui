import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AlertService, EventManager } from 'ng-jhipster';

import { PlanObservation } from './plan-observation.model';
import { PlanObservationPopupService } from './plan-observation-popup.service';
import { PlanObservationService } from './plan-observation.service';

@Component({
    selector: 'jhi-plan-observation-delete-dialog',
    templateUrl: './plan-observation-delete-dialog.component.html'
})
export class PlanObservationDeleteDialogComponent {

    planObservation: PlanObservation;

    constructor(
        private planObservationService: PlanObservationService,
        public activeModal: NgbActiveModal,
        private alertService: AlertService,
        private eventManager: EventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.planObservationService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'planObservationListModification',
                content: 'Deleted an planObservation'
            });
            this.activeModal.dismiss(true);
        });
        this.alertService.success('dcaguiApp.planObservation.deleted', { param : id }, null);
    }
}

@Component({
    selector: 'jhi-plan-observation-delete-popup',
    template: ''
})
export class PlanObservationDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private planObservationPopupService: PlanObservationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.planObservationPopupService
                .open(PlanObservationDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
