import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, AlertService } from 'ng-jhipster';

import { PlanObservation } from './plan-observation.model';
import { PlanObservationPopupService } from './plan-observation-popup.service';
import { PlanObservationService } from './plan-observation.service';
import { RnvPlan, RnvPlanService } from '../rnv-plan';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-plan-observation-dialog',
    templateUrl: './plan-observation-dialog.component.html'
})
export class PlanObservationDialogComponent implements OnInit {

    planObservation: PlanObservation;
    authorities: any[];
    isSaving: boolean;

    rnvplans: RnvPlan[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: AlertService,
        private planObservationService: PlanObservationService,
        private rnvPlanService: RnvPlanService,
        private eventManager: EventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.rnvPlanService.query({page: 0, size: 10000})
            .subscribe((res: ResponseWrapper) => { this.rnvplans = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.planObservation.id !== undefined) {
            this.subscribeToSaveResponse(
                this.planObservationService.update(this.planObservation), false);
        } else {
            this.subscribeToSaveResponse(
                this.planObservationService.create(this.planObservation), true);
        }
    }

    private subscribeToSaveResponse(result: Observable<PlanObservation>, isCreated: boolean) {
        result.subscribe((res: PlanObservation) =>
            this.onSaveSuccess(res, isCreated), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: PlanObservation, isCreated: boolean) {
        this.alertService.success(
            isCreated ? 'dcaguiApp.planObservation.created'
            : 'dcaguiApp.planObservation.updated',
            { param : result.id }, null);

        this.eventManager.broadcast({ name: 'planObservationListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackRnvPlanById(index: number, item: RnvPlan) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-plan-observation-popup',
    template: ''
})
export class PlanObservationPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private planObservationPopupService: PlanObservationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.modalRef = this.planObservationPopupService
                    .open(PlanObservationDialogComponent, params['id']);
            } else {
                this.modalRef = this.planObservationPopupService
                    .open(PlanObservationDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
