import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { PaginationUtil } from 'ng-jhipster';

import { PlanObservationComponent } from './plan-observation.component';
import { PlanObservationDetailComponent } from './plan-observation-detail.component';
import { PlanObservationPopupComponent } from './plan-observation-dialog.component';
import { PlanObservationDeletePopupComponent } from './plan-observation-delete-dialog.component';

import { Principal } from '../../shared';

@Injectable()
export class PlanObservationResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: PaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const planObservationRoute: Routes = [
    {
        path: 'plan-observation',
        component: PlanObservationComponent,
        resolve: {
            'pagingParams': PlanObservationResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dcaguiApp.planObservation.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'plan-observation/:id',
        component: PlanObservationDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dcaguiApp.planObservation.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const planObservationPopupRoute: Routes = [
    {
        path: 'plan-observation-new',
        component: PlanObservationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dcaguiApp.planObservation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'plan-observation/:id/edit',
        component: PlanObservationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dcaguiApp.planObservation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'plan-observation/:id/delete',
        component: PlanObservationDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dcaguiApp.planObservation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
