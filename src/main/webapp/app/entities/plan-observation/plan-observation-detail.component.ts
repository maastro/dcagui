import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { EventManager  } from 'ng-jhipster';

import { PlanObservation } from './plan-observation.model';
import { PlanObservationService } from './plan-observation.service';

@Component({
    selector: 'jhi-plan-observation-detail',
    templateUrl: './plan-observation-detail.component.html'
})
export class PlanObservationDetailComponent implements OnInit, OnDestroy {

    planObservation: PlanObservation;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: EventManager,
        private planObservationService: PlanObservationService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPlanObservations();
    }

    load(id) {
        this.planObservationService.find(id).subscribe((planObservation) => {
            this.planObservation = planObservation;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPlanObservations() {
        this.eventSubscriber = this.eventManager.subscribe(
            'planObservationListModification',
            (response) => this.load(this.planObservation.id)
        );
    }
}
