import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DcaguiSharedModule } from '../../shared';
import {
    PlanObservationService,
    PlanObservationPopupService,
    PlanObservationComponent,
    PlanObservationDetailComponent,
    PlanObservationDialogComponent,
    PlanObservationPopupComponent,
    PlanObservationDeletePopupComponent,
    PlanObservationDeleteDialogComponent,
    planObservationRoute,
    planObservationPopupRoute,
    PlanObservationResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...planObservationRoute,
    ...planObservationPopupRoute,
];

@NgModule({
    imports: [
        DcaguiSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        PlanObservationComponent,
        PlanObservationDetailComponent,
        PlanObservationDialogComponent,
        PlanObservationDeleteDialogComponent,
        PlanObservationPopupComponent,
        PlanObservationDeletePopupComponent,
    ],
    entryComponents: [
        PlanObservationComponent,
        PlanObservationDialogComponent,
        PlanObservationPopupComponent,
        PlanObservationDeleteDialogComponent,
        PlanObservationDeletePopupComponent,
    ],
    providers: [
        PlanObservationService,
        PlanObservationPopupService,
        PlanObservationResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DcaguiPlanObservationModule {}
