import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { PlanObservation } from './plan-observation.model';
import { PlanObservationService } from './plan-observation.service';
@Injectable()
export class PlanObservationPopupService {
    private isOpen = false;
    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private planObservationService: PlanObservationService

    ) {}

    open(component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.planObservationService.find(id).subscribe((planObservation) => {
                planObservation.observationDate = this.datePipe
                    .transform(planObservation.observationDate, 'yyyy-MM-ddThh:mm');
                this.planObservationModalRef(component, planObservation);
            });
        } else {
            return this.planObservationModalRef(component, new PlanObservation());
        }
    }

    planObservationModalRef(component: Component, planObservation: PlanObservation): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.planObservation = planObservation;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
