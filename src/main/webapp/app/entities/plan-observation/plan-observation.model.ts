import { RnvPlan } from '../rnv-plan';
export class PlanObservation {
    constructor(
        public id?: number,
        public observationIdentifier?: string,
        public observationValue?: string,
        public observationDate?: any,
        public rnvPlan?: RnvPlan,
    ) {
    }
}
