import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { FhirTransactionLog } from './fhir-transaction-log.model';
import { FhirTransactionLogService } from './fhir-transaction-log.service';
@Injectable()
export class FhirTransactionLogPopupService {
    private isOpen = false;
    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private fhirTransactionLogService: FhirTransactionLogService

    ) {}

    open(component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.fhirTransactionLogService.find(id).subscribe((fhirTransactionLog) => {
                fhirTransactionLog.date = this.datePipe
                    .transform(fhirTransactionLog.date, 'yyyy-MM-ddThh:mm');
                this.fhirTransactionLogModalRef(component, fhirTransactionLog);
            });
        } else {
            return this.fhirTransactionLogModalRef(component, new FhirTransactionLog());
        }
    }

    fhirTransactionLogModalRef(component: Component, fhirTransactionLog: FhirTransactionLog): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.fhirTransactionLog = fhirTransactionLog;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
