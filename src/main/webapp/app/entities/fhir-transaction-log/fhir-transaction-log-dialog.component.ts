import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, AlertService, DataUtils } from 'ng-jhipster';

import { FhirTransactionLog } from './fhir-transaction-log.model';
import { FhirTransactionLogPopupService } from './fhir-transaction-log-popup.service';
import { FhirTransactionLogService } from './fhir-transaction-log.service';

@Component({
    selector: 'jhi-fhir-transaction-log-dialog',
    templateUrl: './fhir-transaction-log-dialog.component.html'
})
export class FhirTransactionLogDialogComponent implements OnInit {

    fhirTransactionLog: FhirTransactionLog;
    authorities: any[];
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: DataUtils,
        private alertService: AlertService,
        private fhirTransactionLogService: FhirTransactionLogService,
        private eventManager: EventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, fhirTransactionLog, field, isImage) {
        if (event.target.files && event.target.files[0]) {
            const file = event.target.files[0];
            if (isImage && !/^image\//.test(file.type)) {
                return;
            }
            this.dataUtils.toBase64(file, (base64Data) => {
                fhirTransactionLog[field] = base64Data;
                fhirTransactionLog[`${field}ContentType`] = file.type;
            });
        }
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.fhirTransactionLog.id !== undefined) {
            this.subscribeToSaveResponse(
                this.fhirTransactionLogService.update(this.fhirTransactionLog), false);
        } else {
            this.subscribeToSaveResponse(
                this.fhirTransactionLogService.create(this.fhirTransactionLog), true);
        }
    }

    private subscribeToSaveResponse(result: Observable<FhirTransactionLog>, isCreated: boolean) {
        result.subscribe((res: FhirTransactionLog) =>
            this.onSaveSuccess(res, isCreated), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: FhirTransactionLog, isCreated: boolean) {
        this.alertService.success(
            isCreated ? 'dcaguiApp.fhirTransactionLog.created'
            : 'dcaguiApp.fhirTransactionLog.updated',
            { param : result.id }, null);

        this.eventManager.broadcast({ name: 'fhirTransactionLogListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-fhir-transaction-log-popup',
    template: ''
})
export class FhirTransactionLogPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private fhirTransactionLogPopupService: FhirTransactionLogPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.modalRef = this.fhirTransactionLogPopupService
                    .open(FhirTransactionLogDialogComponent, params['id']);
            } else {
                this.modalRef = this.fhirTransactionLogPopupService
                    .open(FhirTransactionLogDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
