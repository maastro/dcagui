import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { EventManager , DataUtils } from 'ng-jhipster';

import { FhirTransactionLog } from './fhir-transaction-log.model';
import { FhirTransactionLogService } from './fhir-transaction-log.service';

@Component({
    selector: 'jhi-fhir-transaction-log-detail',
    templateUrl: './fhir-transaction-log-detail.component.html'
})
export class FhirTransactionLogDetailComponent implements OnInit, OnDestroy {

    fhirTransactionLog: FhirTransactionLog;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: EventManager,
        private dataUtils: DataUtils,
        private fhirTransactionLogService: FhirTransactionLogService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInFhirTransactionLogs();
    }

    load(id) {
        this.fhirTransactionLogService.find(id).subscribe((fhirTransactionLog) => {
            this.fhirTransactionLog = fhirTransactionLog;
        });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInFhirTransactionLogs() {
        this.eventSubscriber = this.eventManager.subscribe(
            'fhirTransactionLogListModification',
            (response) => this.load(this.fhirTransactionLog.id)
        );
    }
}
