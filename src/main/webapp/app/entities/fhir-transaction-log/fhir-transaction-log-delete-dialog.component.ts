import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AlertService, EventManager } from 'ng-jhipster';

import { FhirTransactionLog } from './fhir-transaction-log.model';
import { FhirTransactionLogPopupService } from './fhir-transaction-log-popup.service';
import { FhirTransactionLogService } from './fhir-transaction-log.service';

@Component({
    selector: 'jhi-fhir-transaction-log-delete-dialog',
    templateUrl: './fhir-transaction-log-delete-dialog.component.html'
})
export class FhirTransactionLogDeleteDialogComponent {

    fhirTransactionLog: FhirTransactionLog;

    constructor(
        private fhirTransactionLogService: FhirTransactionLogService,
        public activeModal: NgbActiveModal,
        private alertService: AlertService,
        private eventManager: EventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.fhirTransactionLogService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'fhirTransactionLogListModification',
                content: 'Deleted an fhirTransactionLog'
            });
            this.activeModal.dismiss(true);
        });
        this.alertService.success('dcaguiApp.fhirTransactionLog.deleted', { param : id }, null);
    }
}

@Component({
    selector: 'jhi-fhir-transaction-log-delete-popup',
    template: ''
})
export class FhirTransactionLogDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private fhirTransactionLogPopupService: FhirTransactionLogPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.fhirTransactionLogPopupService
                .open(FhirTransactionLogDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
