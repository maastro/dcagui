
const enum TransactionStatus {
    'SUCCESS',
    'FAILED'

};
export class FhirTransactionLog {
    constructor(
        public id?: number,
        public date?: any,
        public status?: TransactionStatus,
        public planUid?: string,
        public outgoingBundle?: any,
        public reponseBundle?: any,
        public errorMessage?: string,
    ) {
    }
}
