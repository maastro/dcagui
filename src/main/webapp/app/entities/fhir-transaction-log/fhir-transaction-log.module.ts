import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DcaguiSharedModule } from '../../shared';
import {
    FhirTransactionLogService,
    FhirTransactionLogPopupService,
    FhirTransactionLogComponent,
    FhirTransactionLogDetailComponent,
    FhirTransactionLogDialogComponent,
    FhirTransactionLogPopupComponent,
    FhirTransactionLogDeletePopupComponent,
    FhirTransactionLogDeleteDialogComponent,
    fhirTransactionLogRoute,
    fhirTransactionLogPopupRoute,
    FhirTransactionLogResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...fhirTransactionLogRoute,
    ...fhirTransactionLogPopupRoute,
];

@NgModule({
    imports: [
        DcaguiSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        FhirTransactionLogComponent,
        FhirTransactionLogDetailComponent,
        FhirTransactionLogDialogComponent,
        FhirTransactionLogDeleteDialogComponent,
        FhirTransactionLogPopupComponent,
        FhirTransactionLogDeletePopupComponent,
    ],
    entryComponents: [
        FhirTransactionLogComponent,
        FhirTransactionLogDialogComponent,
        FhirTransactionLogPopupComponent,
        FhirTransactionLogDeleteDialogComponent,
        FhirTransactionLogDeletePopupComponent,
    ],
    providers: [
        FhirTransactionLogService,
        FhirTransactionLogPopupService,
        FhirTransactionLogResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DcaguiFhirTransactionLogModule {}
