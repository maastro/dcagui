import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { PaginationUtil } from 'ng-jhipster';

import { FhirTransactionLogComponent } from './fhir-transaction-log.component';
import { FhirTransactionLogDetailComponent } from './fhir-transaction-log-detail.component';
import { FhirTransactionLogPopupComponent } from './fhir-transaction-log-dialog.component';
import { FhirTransactionLogDeletePopupComponent } from './fhir-transaction-log-delete-dialog.component';

import { Principal } from '../../shared';

@Injectable()
export class FhirTransactionLogResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: PaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const fhirTransactionLogRoute: Routes = [
    {
        path: 'fhir-transaction-log',
        component: FhirTransactionLogComponent,
        resolve: {
            'pagingParams': FhirTransactionLogResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dcaguiApp.fhirTransactionLog.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'fhir-transaction-log/:id',
        component: FhirTransactionLogDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dcaguiApp.fhirTransactionLog.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const fhirTransactionLogPopupRoute: Routes = [
    {
        path: 'fhir-transaction-log-new',
        component: FhirTransactionLogPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dcaguiApp.fhirTransactionLog.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'fhir-transaction-log/:id/edit',
        component: FhirTransactionLogPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dcaguiApp.fhirTransactionLog.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'fhir-transaction-log/:id/delete',
        component: FhirTransactionLogDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dcaguiApp.fhirTransactionLog.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
