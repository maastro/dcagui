import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { DateUtils, DataUtils, EventManager } from 'ng-jhipster';
import { DcaguiTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { FhirTransactionLogDetailComponent } from '../../../../../../main/webapp/app/entities/fhir-transaction-log/fhir-transaction-log-detail.component';
import { FhirTransactionLogService } from '../../../../../../main/webapp/app/entities/fhir-transaction-log/fhir-transaction-log.service';
import { FhirTransactionLog } from '../../../../../../main/webapp/app/entities/fhir-transaction-log/fhir-transaction-log.model';

describe('Component Tests', () => {

    describe('FhirTransactionLog Management Detail Component', () => {
        let comp: FhirTransactionLogDetailComponent;
        let fixture: ComponentFixture<FhirTransactionLogDetailComponent>;
        let service: FhirTransactionLogService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [DcaguiTestModule],
                declarations: [FhirTransactionLogDetailComponent],
                providers: [
                    DateUtils,
                    DataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    FhirTransactionLogService,
                    EventManager
                ]
            }).overrideTemplate(FhirTransactionLogDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(FhirTransactionLogDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(FhirTransactionLogService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new FhirTransactionLog(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.fhirTransactionLog).toEqual(jasmine.objectContaining({id:10}));
            });
        });
    });

});
