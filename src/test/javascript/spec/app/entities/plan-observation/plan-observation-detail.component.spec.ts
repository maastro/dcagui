import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { DateUtils, DataUtils, EventManager } from 'ng-jhipster';
import { DcaguiTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { PlanObservationDetailComponent } from '../../../../../../main/webapp/app/entities/plan-observation/plan-observation-detail.component';
import { PlanObservationService } from '../../../../../../main/webapp/app/entities/plan-observation/plan-observation.service';
import { PlanObservation } from '../../../../../../main/webapp/app/entities/plan-observation/plan-observation.model';

describe('Component Tests', () => {

    describe('PlanObservation Management Detail Component', () => {
        let comp: PlanObservationDetailComponent;
        let fixture: ComponentFixture<PlanObservationDetailComponent>;
        let service: PlanObservationService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [DcaguiTestModule],
                declarations: [PlanObservationDetailComponent],
                providers: [
                    DateUtils,
                    DataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    PlanObservationService,
                    EventManager
                ]
            }).overrideTemplate(PlanObservationDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PlanObservationDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PlanObservationService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new PlanObservation(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.planObservation).toEqual(jasmine.objectContaining({id:10}));
            });
        });
    });

});
