import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { DateUtils, DataUtils, EventManager } from 'ng-jhipster';
import { DcaguiTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { RnvPlanDetailComponent } from '../../../../../../main/webapp/app/entities/rnv-plan/rnv-plan-detail.component';
import { RnvPlanService } from '../../../../../../main/webapp/app/entities/rnv-plan/rnv-plan.service';
import { RnvPlan } from '../../../../../../main/webapp/app/entities/rnv-plan/rnv-plan.model';

describe('Component Tests', () => {

    describe('RnvPlan Management Detail Component', () => {
        let comp: RnvPlanDetailComponent;
        let fixture: ComponentFixture<RnvPlanDetailComponent>;
        let service: RnvPlanService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [DcaguiTestModule],
                declarations: [RnvPlanDetailComponent],
                providers: [
                    DateUtils,
                    DataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    RnvPlanService,
                    EventManager
                ]
            }).overrideTemplate(RnvPlanDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RnvPlanDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RnvPlanService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new RnvPlan(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.rnvPlan).toEqual(jasmine.objectContaining({id:10}));
            });
        });
    });

});
