import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { DateUtils, DataUtils, EventManager } from 'ng-jhipster';
import { DcaguiTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { DicaRegistrationEntityDetailComponent } from '../../../../../../main/webapp/app/entities/dica-registration-entity/dica-registration-entity-detail.component';
import { DicaRegistrationEntityService } from '../../../../../../main/webapp/app/entities/dica-registration-entity/dica-registration-entity.service';
import { DicaRegistrationEntity } from '../../../../../../main/webapp/app/entities/dica-registration-entity/dica-registration-entity.model';

describe('Component Tests', () => {

    describe('DicaRegistrationEntity Management Detail Component', () => {
        let comp: DicaRegistrationEntityDetailComponent;
        let fixture: ComponentFixture<DicaRegistrationEntityDetailComponent>;
        let service: DicaRegistrationEntityService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [DcaguiTestModule],
                declarations: [DicaRegistrationEntityDetailComponent],
                providers: [
                    DateUtils,
                    DataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    DicaRegistrationEntityService,
                    EventManager
                ]
            }).overrideTemplate(DicaRegistrationEntityDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(DicaRegistrationEntityDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DicaRegistrationEntityService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new DicaRegistrationEntity(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.dicaRegistrationEntity).toEqual(jasmine.objectContaining({id:10}));
            });
        });
    });

});
