import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { DateUtils, DataUtils, EventManager } from 'ng-jhipster';
import { DcaguiTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { PatientObservationDetailComponent } from '../../../../../../main/webapp/app/entities/patient-observation/patient-observation-detail.component';
import { PatientObservationService } from '../../../../../../main/webapp/app/entities/patient-observation/patient-observation.service';
import { PatientObservation } from '../../../../../../main/webapp/app/entities/patient-observation/patient-observation.model';

describe('Component Tests', () => {

    describe('PatientObservation Management Detail Component', () => {
        let comp: PatientObservationDetailComponent;
        let fixture: ComponentFixture<PatientObservationDetailComponent>;
        let service: PatientObservationService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [DcaguiTestModule],
                declarations: [PatientObservationDetailComponent],
                providers: [
                    DateUtils,
                    DataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    PatientObservationService,
                    EventManager
                ]
            }).overrideTemplate(PatientObservationDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PatientObservationDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PatientObservationService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new PatientObservation(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.patientObservation).toEqual(jasmine.objectContaining({id:10}));
            });
        });
    });

});
